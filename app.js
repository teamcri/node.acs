//https://2adf8d2cc6a83ccdf1bcfbed48519483f1de7382.cloudapp.appcelerator.com



/**
* Module dependencies.
*/

var express = require('express'),
routes = require('./routes'),
http = require('http'),
twitter = require('twitter'),
Instagram = require('instagram-node-lib'),
util = require('util'),
path = require('path'),
requestMethod = require('request');

var app = express();

var twit = new twitter({
  consumer_key: 'Cnxrl8pMOJoTFpt1J1L87P2lV',
  consumer_secret: 'Jq8I5C6qnIZwUkn4MFkB1tWNNztwTS4cHHeich0P0dwiz70ogP',
  access_token_key: '45621774-NrlLZAmqbaMCmLVIXAGfMwbNBJ7kMTng3G6A2Q9YV',
  access_token_secret: 'FRVzxQCjB1LiGYpnmqS4bSGlnsmMc7BDDgJaDd0YF6K2z'
});

Instagram.set('client_id', 'e2815442f34949b2a14c4d4c59e7d573');
Instagram.set('client_secret', '933c599472db44a3abf04bedb826587d');
Instagram.set('callback_url', 'https://2adf8d2cc6a83ccdf1bcfbed48519483f1de7382.cloudapp.appcelerator.com/insta');


// all environments
app.set('port', process.env.PORT || 1337);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(__dirname+'/public'));

// development only
if ('development' == app.get('env')) {
app.use(express.errorHandler());
}

var eServer = http.createServer(app);
eServer.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

var io = require('socket.io').listen(eServer);
var appstream = io.of('/appstream').on('connection', function (socket) {
              socket.on('params', function(parameters){
                console.log(parameters);
                var reqURL = 'https://script.google.com/macros/s/AKfycby4CSJvbvppFFMYkeYWxlZC6UsQcbMnpTFCYP-yma5gYjsLwpI/exec?'+parameters;
                  requestMethod(reqURL, function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                      console.log(body); 
                      socket.emit('new_posts', body);
                    }else{console.log('Node.ACS - there was a request error while sending past posts: '+response.statusCode); }
                  });
              });
});

//routes
app.get('/', routes.index);
app.get('/twitter', routes.twitter);
app.post('/appstreamupdate', function(req, res) {
   appstream.emit('new_posts', JSON.stringify(req.body));
  res.contentType('text/plain');
   res.send('stream updated');
});
app.get('/:stream-page', function(req, res) {
 res.render('layout-stream', { title: 'Social Stream'});
});

var last_inst_post;
app.post('/insta', function (request, response) {

  console.log(request.body);
  console.log(last_inst_post);

   // Instagram.subscriptions.handshake(request, response); 

    requestMethod('https://api.instagram.com/v1/tags/raclife/media/recent?client_id=e2815442f34949b2a14c4d4c59e7d573&count=1', function (error, response, body) {
        if (!error && response.statusCode == 200) {


            var content = JSON.parse(body).data;
            console.log("post content     "+body);

            if(last_inst_post == content[0].id){
                //duplicate
                console.log("duplicate post");
            }else{

                var created_at, hashtags, location, link, img_stdr, img_thmb, id, text, username, img_profile, mentions;
                    created_at = new Date (parseInt(content[0].created_time)*1000);
                    created_at_seconds = parseInt(content[0].created_time)*1000;
                    hashtags = JSON.stringify(content[0].tags);
                    location = content[0].location;
                    link = content[0].link;
                    img_stdr = content[0].images.standard_resolution.url;
                    img_thmb = content[0].images.thumbnail.url;
                    id = content[0].id;last_inst_post=content[0].id;
                    text = encodeURIComponent(content[0].caption.text);
                    username = content[0].user.username;
                    img_profile = content[0].user.profile_picture;
                    mentions = content[0].users_in_photo;

                var fullInstURL = 'https://script.google.com/macros/s/AKfycby4CSJvbvppFFMYkeYWxlZC6UsQcbMnpTFCYP-yma5gYjsLwpI/exec?platform=Instagram&created_at=' + created_at.toString() +'&created_at_seconds='+created_at_seconds.toString()+'&hashtags='+hashtags+'&coordinates='+location+'&link='+link+'&img_stdr='+img_stdr+'&img_thmb='+img_thmb+'&id='+id+'&text='+encodeURIComponent(text)+'&username='+username+'&img_profile='+img_profile+'&mentions='+mentions;
console.log(fullInstURL);

var options = {
    url: fullInstURL,
    followAllRedirects: true
};
                requestMethod.post(options, function (error, response, body) {
                    if (!error) {
                        console.log(body);
                
                    } else {
                        console.log('Node.ACS - there was a request error while updating Instagram stream: ' + response.statusCode);
                        console.log(body);
                        console.log(response.body);
                    }
                }); // end g apps request

              }//end not duplicate

         
        } // end if there is an instagram response

    }); //end request to instagram


}); //end post



///--------put new tweets in archive


twit.stream('statuses/filter', { track: '#raclife,#trueblue5k,#soarwell,#raclifetesting', follow: '45621774' }, function(stream) {//ids gsucampusrec 45621774,golf course 928246285,wellness 469585107, IM 61518817
stream.on('data', function(tweet) {
	
if(tweet.hasOwnProperty('delete')){
	//deleted tweet notification - ignore
  console.log(tweet);
}else{

var fullURL,
urls="",
hashtags="",
coordinates="",
screen_name,
mentions="",
media="",
native_retweet=false,
reply;

native_retweet = (tweet.hasOwnProperty('retweeted_status') == true) ? true : false;
console.log(tweet.toString());
reply = (tweet.in_reply_to_status_id_str != 'null') ? tweet.in_reply_to_status_id_str : false;
hashtags= (tweet.entities.hashtags.length != 0 && tweet.entities.hashtags != []) ? JSON.stringify(tweet.entities.hashtags) : "";
urls= (tweet.entities.urls.length != 0  && tweet.entities.urls != []) ? JSON.stringify(tweet.entities.urls) : "";
mentions = (tweet.entities.user_mentions.length != 0 && tweet.entities.user_mentions != []) ? JSON.stringify(tweet.entities.user_mentions) : "";
media = (tweet.entities.media != undefined && tweet.entities.media != []) ? JSON.stringify(tweet.entities.media) : "";
coordinates = (tweet.coordinates != undefined && tweet.coordinates != null) ? JSON.stringify(tweet.coordinates) : "";

fullURL = 'https://script.google.com/macros/s/AKfycby4CSJvbvppFFMYkeYWxlZC6UsQcbMnpTFCYP-yma5gYjsLwpI/exec?platform=Twitter&id='+tweet.id_str+'&created_at='+tweet.created_at+'&screen_name='+tweet.user.screen_name+'&hashtags='+hashtags+'&mentions='+mentions+'&reply='+reply+'&urls='+urls+'&text='+encodeURIComponent(tweet.text)+'&coordinates='+coordinates+'&media='+media+'&user_img='+tweet.user.profile_image_url+'&native_retweet='+native_retweet;
var options = {
    url: fullURL,
    followAllRedirects: true
};
console.log(fullURL);

	requestMethod.post(options, function (error, response, body) {
	if (!error && response.statusCode == 200) {
	console.log(body); 
  console.log(response.body); 
	}else{console.log('Node.ACS - there was a request error while updating Twitter stream: '+response.statusCode);console.log(body); }
	});
}
});
stream.on('error', function(error, code) {
	console.log(error + ": " + code);
  console.log(response);
});

});// twit.stream





//Instagram.subscriptions.unsubscribe_all();

//Instagram.tags.unsubscribe({ id: 4327176 });

// [ { changed_aspect: 'media',
//        object: 'tag',
//        object_id: 'raclife',
//        time: 1398362419,
//        subscription_id: 4326541,
//        data: {} } ]

//{ object: 'tag',
//  object_id: 'raclife',
//  aspect: 'media',
//  callback_url: 'https://2adf8d2cc6a83ccdf1bcfbed48519483f1de7382.cloudapp.appcelerator.com/insta',
//  type: 'subscription',
//  id: '4327176' }


/*var instObj = {
  object: 'tag',
  object_id: 'raclife',
  aspect: 'media',
  callback_url: 'https://2adf8d2cc6a83ccdf1bcfbed48519483f1de7382.cloudapp.appcelerator.com/insta-post',
  complete: function(data, pagination){
      // data is a javascript object/array/null matching that shipped Instagram
      // when available (mostly /recent), pagination is a javascript object with the pagination information
    console.log(data);
    },
  error: function(errorMessage, errorObject, caller){
      // errorMessage is the raised error message
      // errorObject is either the object that caused the issue, or the nearest neighbor
      // caller is the method in which the error occurred
    console.log(errorMessage);
    console.log(errorObject);
    console.log(caller);
    }
};

Instagram.tags.subscribe(instObj);*/






///--------Device Connection to Live Stream of custom feed
        // io.sockets.on('connection', function(socket) {

        //     socket.on('stream', function(searchTerm){

        //         twit.stream('statuses/filter', { track: searchTerm }, //streams the search terms sent from the device
        //             function(stream) {
        //                 stream.on('data', function(tweet) {
        //                   if(tweet.text !== null){
        //                     socket.emit('tweet', tweet.text);
        //                   }
                          
        //                 });
        //                 stream.on('error', function(error, code) {
        //                     console.log("Stream Error: " + error + ": " + code);
        //                 });
        //             }


        //         );
        //     });
        // });







